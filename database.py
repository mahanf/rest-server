from flask import jsonify
import sqlite3, datetime


db = "wetter.db"


#Anlegen der Datenbank - wird nur beim ersten Mal benötigt
#conn = sqlite3.connect(db)
#conn.execute('CREATE TABLE wetterdata (station TEXT, date TEXT, temperature NUMERIC)')
#conn.close


def read_db():
    # Übergabe der Werte an die Datenbank
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
    cursor.execute("SELEcT * FROM wetterdata")
    conn.commit()
    result = cursor.fetchall()
    conn.close()
    return result


def insert_db(station, temperature):
    # Übergabe der Werte an die Datenbank
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
    date = datetime.datetime.now()
    cursor.execute("INSERT INTO wetterdata(station ,date , temperature) VALUES (?,?,?)",(station,date,temperature))
    conn.commit()
    conn.close()


def clear_db():
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
    cursor.execute("DELETE FROM wetterdata")
    conn.commit()
    conn.close()


