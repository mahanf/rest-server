import paho.mqtt.client as mqtt
import json
import time,random
import temperatur

MQTT_Topic=("Newbie",0)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(MQTT_Topic)

client = mqtt.Client()
client.on_connect = on_connect
client.connect("10.14.213.223", 1883, 60)

#Creates a random temperature and publishes JSON to "wetter_test"-topic
# every 10 seconds
while(True):
    temp_value = json.dumps({"station": "Wetter1",
                "temperature": temperatur.temperatur(10) })

    client.publish(MQTT_Topic, temp_value)
    time.sleep(5)
