from flask import Flask, jsonify, request, render_template
import database


app = Flask(__name__)
test_key = "jashdjkfASDFsflkk34sdkfj"


def check_apikey(apikey):
    # Key wird eigentlich mit der Datenbank abgegliechen
    if apikey == test_key:
        return True
    else:
        return False


@app.route('/')
def index():
    return render_template('index.html')


@app.route("/status")
def status():
    return jsonify(server="running")


@app.route("/info")
def info():
    database.read_db()


@app.route("/wetter",methods=['POST'])
def wetter_post():
    # Prüfe den Key
    apikey = request.headers.get('key')
    if check_apikey(apikey) is True:
        values = request.get_json()
        station = values['station']
        temperature = values['temperature']
        # Übergabe der Werte in die Datenbank
        database.insert_db(station, temperature)
        return jsonify(server="running", station=station, temperature=temperature, methode="POST")
    else:
        return jsonify(server="running", info="Fehler-APIKEY")


@app.route("/wetter",methods=['GET'])
def wetter_get():
    # Prüfe den Key
    apikey = request.headers.get('key')
    if check_apikey(apikey) is True:
        result = database.read_db()
        return jsonify(server="running", info=result)
    else:
        return jsonify(server="running", info="Fehler-APIKEY")


@app.route("/wetter",methods=['DELETE'])
def wetter_delete():
    # Prüfe den Key
    apikey = request.headers.get('key')
    if check_apikey(apikey) is True:
        database.clear_db()
        return jsonify(server="running", info="Values cleared from DB")
    else:
        return jsonify(server="running", info="Fehler-APIKEY")


# Start des Web-Dienstes
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)