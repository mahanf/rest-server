## Eingabe der Starttemperatur durch Nutzer
## Programm gibt Zufallswerte +- 5 Grad aus
## Alle 5 Sekunden wir eine neue Temperatur erzeugt

import random

def temperatur(start_wert):
    
    zufall = random.randint(-5,5)
    temperatur_wert = start_wert+zufall

    return temperatur_wert

